#include <Arduino.h>
#include <ESP8266HTTPClient.h>
#include <ESP8266httpUpdate.h>
#include <ESP8266WebServer.h>
#include <ESP8266WiFi.h>
#include <FastLED.h>
#include <Pinger.h>
#include <Task.h>
#include <WiFiClientSecureBearSSL.h>
#include <WiFiManager.h>
extern "C"
{
#include <lwip/icmp.h> // needed for icmp packet definitions
}

#define PING_SECONDS 10  // How often to ping
#define NUM_LEDS 4


/*******************************
 * The code runs an asynchronous task called `pingTask`, which pings a bunch of hosts and changes the state if any of
 * them don't respond. It also runs a synchronous function called `breathe`, which changes an LED depending on the ping
 * state.
 */

IPAddress broadcastIP(192, 168, 10, 255);
Pinger pinger;
TaskManager taskManager;
WiFiManager wifiManager;


enum states {
    NO_INTERNET,
    NO_DNS,
    NO_ROUTER,
    ALL_GOOD
};

states state = ALL_GOOD;

// Whether we're currently pinging or we're done.
bool pinging = false;

// Which host we're pinging right now.
int pingedHost = 0;
bool results[4] = { true, true, true, true };


void pingTask(uint32_t deltaTime);
void updateTask(uint32_t deltaTime);

FunctionTask taskPing(pingTask, MsToTaskTime(PING_SECONDS * 1000));
FunctionTask taskUpdate(updateTask, MsToTaskTime(3600 * 1000));
CRGBArray<NUM_LEDS> leds;


void logUDP(String message, IPAddress ip) {
    if (WiFi.status() != WL_CONNECTED) {
        return;
    }

    WiFiUDP Udp;

    // Listen with `nc -kul 37243`.
    Udp.beginPacket(ip, 37243);
    Udp.write(("(" PROJECT_NAME  ": " + String(millis()) + " - " + WiFi.localIP().toString() + ") " + ": " + message + "\n").c_str());
    Udp.endPacket();
}


void doHTTPUpdate() {
    digitalWrite(LED_BUILTIN, LOW);
    if (WiFi.status() != WL_CONNECTED) {
        return;
    }
    logUDP("[update] Looking for an update from v" VERSION ".", broadcastIP);

    std::unique_ptr<BearSSL::WiFiClientSecure>client(new BearSSL::WiFiClientSecure);
    client->setFingerprint(OTA_FINGERPRINT);
    t_httpUpdate_return ret = ESPhttpUpdate.update(*client, "https://" OTA_HOSTNAME "/" PROJECT_NAME "/", VERSION);
    switch (ret) {
    case HTTP_UPDATE_FAILED:
        logUDP("[update] Update failed.", broadcastIP);
        break;
    case HTTP_UPDATE_NO_UPDATES:
        logUDP("[update] No update from v" VERSION ".", broadcastIP);
        break;
    case HTTP_UPDATE_OK:
        logUDP("[update] Update ok.", broadcastIP);
        break;
    }
    digitalWrite(LED_BUILTIN, HIGH);
}


void setColor(uint8_t h, uint8_t s, uint8_t v) {
    fill_solid(leds, NUM_LEDS, CHSV(h, s, v));
    FastLED.show();
}


void breathe(states s) {
    uint8_t hue;
    float degtouint = 360 / 255;
    float val = ((exp(sin(millis() / 2000.0 * PI)) - 0.36787944) * 108.0);
    val = max(3, (int)val);
    switch (s) {
    case NO_INTERNET:
        hue = 0 / degtouint;
        setColor(hue, 255, val);
        break;
    case NO_DNS:
        hue = 34 / degtouint;
        setColor(hue, 255, val);
        break;
    case NO_ROUTER:
        hue = 290 / degtouint;
        setColor(hue, 255, val);
        break;
    case ALL_GOOD:
        hue = 90 / degtouint;
        // Be dark when we have internet.
        val = 0;
        setColor(hue, 255, val * 0.4);
        break;
    }
}


void updateTask(uint32_t deltaTime) {
    doHTTPUpdate();
}


void pingTask(uint32_t deltaTime) {
    if (pinging) {
        return;
    }

    pinger.OnEnd(pingEnd);

    pinging = true;
    pingedHost++;
    if (pingedHost > 3) {
        pingedHost = 0;
    }

    switch (pingedHost) {
    case 0:
        Serial.printf(
            "\r\n\r\nPinging default gateway with IP %s\r\n",
            WiFi.gatewayIP().toString().c_str());
        if (pinger.Ping(WiFi.gatewayIP(), 2) == false) {
            Serial.println("Error during ping command.");
            pinging = false;
            state = NO_INTERNET;
        }
        break;
    case 1:
        Serial.printf("\r\n\r\nPinging google.com\r\n");
        if (pinger.Ping("www.stavros.io", 2) == false) {
            Serial.println("Error during ping command.");
            pinging = false;
            state = NO_INTERNET;
        }
        break;
    case 2:
        Serial.printf("\r\n\r\nPinging ip 1.1.1.1\r\n");
        if (pinger.Ping(IPAddress(1, 1, 1, 1), 2) == false) {
            Serial.println("Error during ping command.");
            pinging = false;
            state = NO_INTERNET;
        }
        break;
    case 3:
        Serial.printf("\r\n\r\nPinging ip of projects.stavros.io\r\n");
        if (pinger.Ping(IPAddress(195, 201, 40, 251), 2) == false) {
            Serial.println("Error during ping command.");
            pinging = false;
            state = NO_INTERNET;
        }
        break;
    }

    Serial.println("Ping done.");
}


bool pingEnd(const PingerResponse & response) {
    float loss = 100;
    if (response.TotalReceivedResponses > 0) {
        loss = (response.TotalSentRequests - response.TotalReceivedResponses) * 100 / response.TotalSentRequests;
    }

    results[pingedHost] = (response.TotalReceivedResponses > 0) ? true : false;

    if (results[0] == false) {
        state = NO_ROUTER;
    } else if (
        results[0] == true &&
        results[1] == false &&
        results[2] == true &&
        results[3] == true
    ) {
        state = NO_DNS;
    } else if (
        results[0] == true &&
        results[1] == false &&
        results[2] == false &&
        results[3] == false
    ) {
        state = NO_INTERNET;
    } else {
        state = ALL_GOOD;
    }

    // Print packet trip data
    Serial.printf(
        "Ping statistics for %s:\r\n",
        response.DestIPAddress.toString().c_str());
    Serial.printf(
        "    Packets: Sent = %lu, Received = %lu, Lost = %lu (%.2f%% loss),\r\n",
        response.TotalSentRequests,
        response.TotalReceivedResponses,
        response.TotalSentRequests - response.TotalReceivedResponses,
        loss);

    // Print time information
    if (response.TotalReceivedResponses > 0) {
        Serial.printf("Approximate round trip times in milli-seconds:\r\n");
        Serial.printf(
            "    Minimum = %lums, Maximum = %lums, Average = %.2fms\r\n",
            response.MinResponseTime,
            response.MaxResponseTime,
            response.AvgResponseTime);
    }

    // Print host data
    Serial.printf("Destination host data:\r\n");
    Serial.printf(
        "    IP address: %s\r\n",
        response.DestIPAddress.toString().c_str());
    if (response.DestMacAddress != nullptr) {
        Serial.printf(
            "    MAC address: " MACSTR "\r\n",
            MAC2STR(response.DestMacAddress->addr));
    }
    if (response.DestHostname != "") {
        Serial.printf(
            "    DNS name: %s\r\n",
            response.DestHostname.c_str());
    }

    pinging = false;
    return true;
}


void setup() {
    Serial.begin(115200);

    FastLED.addLeds<NEOPIXEL, 0>(leds, NUM_LEDS);
    FastLED.clear(true);

    pinMode(LED_BUILTIN, OUTPUT);
    digitalWrite(LED_BUILTIN, HIGH);

    WiFi.hostname("InternetCube");
    wifiManager.autoConnect("InternetCube");
    broadcastIP = WiFi.localIP();
    broadcastIP[3] = 255;

    doHTTPUpdate();

    taskManager.StartTask(&taskUpdate);
    taskManager.StartTask(&taskPing);
}


void loop() {
    delay(1);
    breathe(state);
    taskManager.Loop();
}
